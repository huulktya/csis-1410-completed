package ArrayAccess;


import javax.swing.JOptionPane;

public class ArrayAccess
{

	private int num; // number entered from the keyboard
	private int index = 0; // index into the array
	private int array[] = new int[10];

	// default constructor
	public ArrayAccess()
	{
	}// end constructor

	public void run()
	{
		enterData();
		findElementByValue();
		findElementByIndex();
	}// end test()

	private void enterData()
	{
		boolean successful = false;
		while (!successful)
		{
			try
			{
				num = Integer
						.parseInt(JOptionPane
								.showInputDialog("Please enter a value to put at index: "
										+ index));
				array[index] = num;
				index++;
				successful = true;
			}
			catch (NumberFormatException e)
			{
				JOptionPane.showMessageDialog(null,
						"Please enter only integer values", "Invalid Input",
						JOptionPane.ERROR_MESSAGE);
			}
			catch (ArrayIndexOutOfBoundsException e)
			{
				JOptionPane.showMessageDialog(null,
						"Array may contain only 10 elements", "Array full",
						JOptionPane.ERROR_MESSAGE);
				// If the array is full we can't add data to it so the program
				// will be stuck looping unless I do this:
				successful = true;
			}
		}
		/*
		 * Create a try block in which the application assigns the number
		 * entered from the keyboard to the next index in the array, then
		 * increments instance variable index.
		 */

		/*
		 * Write catch handlers that catch the two types of exceptions that the
		 * previous try block might throw (NumberFormatException and
		 * ArrayIndexOutOfBoundsException), and display appropriate messages in
		 * error message dialogs.
		 */
	}// end enterData()

	private void findElementByValue()
	{
		boolean successful = false;
		while (!successful)
		{
			try
			{
				boolean found = false;
				String message = "";
				num = Integer.parseInt(JOptionPane
						.showInputDialog("Please enter a value to find:"));
				for (int i = 0; i < array.length; i++)
				{
					if (array[i] == num)
					{
						message = message + i + "\n";
						found = true;
					}
				}
				if (!found)
				{
					throw new NumberNotFoundException();
				}
				JOptionPane
						.showMessageDialog(null, "Found the value at:\n"
								+ message, "Found it!",
								JOptionPane.INFORMATION_MESSAGE);
				successful = true;
			}
			catch (NumberNotFoundException e)
			{
				JOptionPane.showMessageDialog(null,
						"Number not found in array", "Not Found",
						JOptionPane.ERROR_MESSAGE);
			}
			catch (NumberFormatException e)
			{
				JOptionPane.showMessageDialog(null,
						"Please enter only integer values", "Invalid Input",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		/*
		 * Create a try block in which the application reads from the keyboard
		 * the number the user wants to find in the array, then searches the
		 * current array contents for the number. If the number is found,
		 * display all the indices in which the number was found. If the number
		 * is not found, a NumberNotFoundException should be thrown.
		 */

		/*
		 * Write catch handlers that catch the two types of exceptions that the
		 * try block might throw (NumberFormatException and
		 * NumberNotFoundException), and display appropriate messages in error
		 * message dialogs.
		 */
	}// end findElementByValue()

	private void findElementByIndex()
	{
		boolean successful = false;
		while (!successful)
		{
			try
			{
				num = Integer.parseInt((JOptionPane
						.showInputDialog("Please enter the index to find")));
				if (num > index)
				{
					throw new ArrayIndexOutOfBoundsException();
				}
				int val = array[num];
				JOptionPane.showMessageDialog(null, "The value at " + num
						+ " is " + val, "Found it!",
						JOptionPane.INFORMATION_MESSAGE);
				successful = true;
			}
			catch (NumberFormatException e)
			{
				JOptionPane.showMessageDialog(null,
						"Please enter only integer values", "Invalid Input",
						JOptionPane.ERROR_MESSAGE);
			}
			catch (ArrayIndexOutOfBoundsException e)
			{
				JOptionPane.showMessageDialog(null, "Index not found",
						"Index Out of Bounds", JOptionPane.ERROR_MESSAGE);

			}
		}
		/*
		 * Create a try block in which the application reads from the keyboard
		 * the index of a value in the array, then displays the value at that
		 * index. If the index input by the user is not a number a
		 * NumberFormatException should be thrown. If the number input by the
		 * user is outside the array bounds or represents an element in which
		 * the application has not stored a value, an
		 * ArrayIndexOutOfBoundsException should be thrown.
		 */

		/*
		 * Write catch handlers that catch the two types of exceptions the try
		 * block might throw (NumberFormatException and
		 * ArrayIndexOutOfBoundsException), and display appropriate messages in
		 * error message dialogs.
		 */
	}// end findElementByIndex()

} // end class ArrayAccess

