package PigLatin;
/********************************************************
 * Project : CSIS1410 
 * File : PigLatinForClass.java 
 * Name : Trenton Maki 
 * Date : Oct 22,
 * 2013 Description : A pig latin translator
 ********************************************************/
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;


/**
 * @author Huulktya
 */
public class PigLatinForClass extends JFrame
{

	private JPanel contentPane;
	private JTextArea inputArea;
	private JScrollPane scrollPane_1;
	private JTextArea outputArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					PigLatinForClass frame = new PigLatinForClass();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PigLatinForClass()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[grow]", "[grow][][grow]"));

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "cell 0 0,grow");

		inputArea = new JTextArea();
		inputArea.setLineWrap(true);
		scrollPane.setViewportView(inputArea);

		JButton convertButton = new JButton("Convert to Pig Latin");
		convertButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				convertToPigLatin();
			}
		});
		contentPane.add(convertButton, "cell 0 1,growx");

		scrollPane_1 = new JScrollPane();
		contentPane.add(scrollPane_1, "cell 0 2,grow");

		outputArea = new JTextArea();
		outputArea.setEditable(false);
		outputArea.setLineWrap(true);
		scrollPane_1.setViewportView(outputArea);
	}

	/****************************************************
	 * Method : convertToPigLatin 
	 * Purpose : Takes a large chunk of text from input and
	 *  converts each word to Pig Latin, maintaining end of 
	 *  sentence punctuation 
	 ****************************************************/
	protected void convertToPigLatin()
	{
		String input = inputArea.getText();
		String output = "";
		String[] sentences = input.split("[!\\.\\?]");

		int indexOfSentenceBreak = 0;
		for (String sentence : sentences)
		{
			indexOfSentenceBreak += sentence.length();
			String[] words = sentence.split(" ");
			for (String word : words)
			{
				word = word.trim();
				output = output + " " + convertToLatin(word);
			}
			try
			{
				output = output + "" + input.charAt(indexOfSentenceBreak);
				// If this fails that means there was no end of sentence
				// punctuation.
			}
			catch (StringIndexOutOfBoundsException e)
			{
				JOptionPane.showMessageDialog(null,
						"Why aren't you ending sentences with punctuation?");
			}
			//Because the punctuation mark adds one more to the index
			indexOfSentenceBreak++;
		}
		outputArea.setText(output.trim());
	}

	/****************************************************
	 * Method : convertToLatin 
	 * 
	 * Purpose : Converts the word to pig latin
	 * 
	 * @param word the word to convert
	 * @return the word translated into pig latin 
	 ****************************************************/
	private String convertToLatin(String word)
	{
		String newWord = word;

		if (newWord.isEmpty())
		{
			return word;
		}
		else if (newWord.length() < 2)
		{
			return word;
		}
		else if (!newWord.matches("[a-zA-Z]+"))
		{
			// Can't deal with numbers and symbols
			return word;
		}
		newWord = newWord.replaceAll("\\s+", ""); // remove spaces and new lines
		char firstLetter = newWord.charAt(0);
		newWord = newWord.substring(1).concat(Character.toString(firstLetter))
				.concat("ay");
		if (Character.isUpperCase(firstLetter))
		{
			newWord = newWord.toLowerCase();
			String newFirst = newWord.substring(0, 1);
			newFirst = newFirst.toUpperCase();
			newWord = newFirst.concat(newWord.substring(1));
		}
		return newWord.trim();
	}
}
