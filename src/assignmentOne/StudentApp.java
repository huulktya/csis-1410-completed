/********************************************************
 * Project : Arraylist Of Students
 * File : Student.java 
 * Name : Trenton Maki 
 * Date: 9/29/13 
 * Description : This is the main app of the assignment, it supports
 * adding, deleting, and finding students and it can display all students
 ********************************************************/
package assignmentOne;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class StudentApp
{
	private StudentList list;
	private List<Integer> usedSNumbers;

	/****************************************************
	 * Method : main
	 * Purpose : The main function for this app, displays the menu
	 * chooses results based on user input
	 * 
	 * @param args
	 *            the arguments passed to this function
	 ****************************************************/
	public static void main(String... args)
	{
		StudentApp app = new StudentApp();
		app.go();
	}

	private void go()
	{
		list = new StudentList();
		usedSNumbers = new ArrayList<Integer>();
		System.out.println("\nWelcome to the directory of students!\n");
		boolean continuing = true;
		Scanner scanner = new Scanner(System.in);
		while (continuing)
		{
			System.out.println("Please enter your command below:");
			System.out.println("1. Add a student");
			System.out.println("2. Find a student");
			System.out.println("3. Delete a student");
			System.out.println("4. Display all students");
			System.out.println("5. Display total number of students");
			System.out
					.println("6. Quit (You can also quit any operation by entering -1)");
			int choice = 0;

			choice = nextInt(scanner);

			switch (choice)
			{
			case 1:
				addStudent(scanner);
				break;
			case 2:
				findStudent(scanner);
				break;
			case 3:
				deleteStudent(scanner);
				break;
			case 4:
				displayAllStudents();
				break;
			case 5:
				displayNumberOfStudents();
				break;
			case 6:
				System.out.println("Goodbye!");
				continuing = false;
				break;
			default:
				System.out
						.println("Please enter a number in the range of 1 to 6 inclusive\n");
				break;
			}
		}
		scanner.close();
	}

	/****************************************************
	 * Method : displayNumberOfStudents 
	 * Purpose : Displays the total number of
	 * students
	 ****************************************************/
	private void displayNumberOfStudents()
	{
		int numOfStudents = Student.getCount();
		if (numOfStudents == 0)
		{
			System.out.println("There are no students\n");
		}
		else if (numOfStudents == 1)
		{
			System.out.println("There is only one student\n");
		}
		else
		{
			System.out.println("There are: " + numOfStudents + " students\n");
		}
	}

	/**
	 * **************************************************
	 *  Method : displayAllStudents 
	 * Purpose : Display all of the students registered
	 */
	private void displayAllStudents()
	{
		System.out.println();
		List<Student> students = list.getAllStudents();
		if (students.isEmpty())
		{
			System.out.println("No students to display!");
		}
		else
		{
			for (Student student : students)
			{
				System.out.println(student);
			}
		}
		System.out.println();
	}

	/**
	 * **************************************************
	 * Method  : deleteStudent
	 * Purpose : Delete a student from the list
	 * 
	 * @param scanner
	 *            the scanner that we should listen to
	 */
	private void deleteStudent(Scanner scanner)
	{
		System.out
				.println("Please enter the students sNumber, full name, first name, or last name:");
		String input = scanner.nextLine();
		if (isNegOne(input))
		{
			System.out.println("Quitting...\n");
			return;
		}
		Student studentDeleted = list.findStudent(input);
		boolean succeed = list.deleteStudent(input);
		if (succeed)
		{
			System.out.println("This student was deleted: " + studentDeleted
					+ "\n");
		}
		else
		{
			System.out
					.println("Sorry! we couldn't find the student you wanted,"
							+ " or something else went wrong\n");
		}
	}

	/**
	 * ************************************************** 
	 * Method  : findStudent
	 * Purpose : Finds a student based on the information provided
	 * 
	 * @param scanner
	 *            The scanner to listen to input from
	 */
	private void findStudent(Scanner scanner)
	{
		System.out
				.println("Please enter the students sNumber, full name, first name,"
						+ " or last name (please\nnote that the student that was most"
						+ " recently added will be the one shown if\nthere are duplicates):");
		while (true)
		{
			String input = scanner.nextLine();
			if (isNegOne(input))
			{
				System.out.println("Quiting...\n");
				return;
			}
			Student student = list.findStudent(input);
			if (student == null)
			{
				System.out
						.println("Sorry, we couldn't find any students matching: "
								+ input + "\n");
				return;
			}
			System.out.println("Here is the student's information:");
			System.out.println(student + "\n");
			return;
		}
	}

	/**
	 * ************************************************** 
	 * Method  : addStudent
	 * Purpose : Adds a student to the current list of students
	 * 
	 * @param scanner
	 *            the scanner to collect data from
	 */
	private void addStudent(Scanner scanner)
	{
		String firstName;
		String lastName;
		String major;
		double gpa;
		int sNumber;

		System.out.println("Please enter the student's first name:");
		firstName = scanner.next();
		if (isNegOne(firstName))
		{
			System.out.println("Quiting...\n");
			return;
		}

		System.out.println("Please enter the student's last name:");
		lastName = scanner.next();
		if (isNegOne(lastName))
		{
			System.out.println("Quiting...\n");
			return;
		}

		System.out.println("Please enter the student's major:");
		major = scanner.next();
		if (isNegOne(major))
		{
			System.out.println("Quiting...\n");
			return;
		}

		System.out.println("Please enter the student's GPA:");
		gpa = nextDouble(scanner);
		if (gpa == -1)
		{
			System.out.println("Quiting...\n");
			return;
		}

		sNumber = getSNumber(scanner);
		if (sNumber == -1)
		{
			System.out.println("Quiting...\n");
			return;
		}

		Student student = new Student(firstName, lastName, major, gpa, sNumber);
		list.addStudent(student);
		System.out.println("Student added!\n");
	}

	private int getSNumber(Scanner scanner)
	{
		int sNumber;
		System.out
				.println("Please enter the student's sNumber type \"gen\" to have one generated:");
		while (true)
		{
			String choiceString = scanner.next();
			if (choiceString.equals("gen"))
			{
				sNumber = genSNumber();
				System.out.println("The sNumber generated was: " + sNumber);
				break;
			}
			else if (isInt(choiceString))
			{
				sNumber = Integer.parseInt(choiceString);
				if(!usedSNumbers.contains(sNumber)) {
					usedSNumbers.add(sNumber);
					break;					
				} else {
					System.out.println("Sorry, that sNumber has already been used");
				}
			}
			else
			{
				if (!choiceString.isEmpty())
				{
					System.out.println("Please enter a number or \"gen\"");
				}
			}
		}
		return sNumber;
	}

	/**
	 * ************************************************** 
	 * Method : genSNumber
	 * Purpose : Generate an sNumber randomly for the user. For the purposes of
	 * this app an sNumber is any number between 100,000 and 999,999 inclusive
	 * 
	 * @return a randomly generated number between 100,000 and 999,999 inclusive
	 */
	private int genSNumber()
	{
		Random random = new Random();
		int sNumber = random.nextInt((int) Math.pow(10, 6));
		// This is just here to assure that the sNumber is a 6 digit number
		while (sNumber < Math.pow(10, 5) || usedSNumbers.contains(sNumber))
		{
			sNumber = random.nextInt((int) Math.pow(10, 6));
		}
		usedSNumbers.add(sNumber);
		return sNumber;
	}

	/**
	 * ************************************************** 
	 * Method  : isNegOne
	 * Purpose : A convenience method for testing if a String is in fact the
	 * number -1
	 * 
	 * @param firstName
	 * @return a boolean representing whether or not a given string is actually
	 *         -1
	 */
	private boolean isNegOne(String firstName)
	{
		if (isInt(firstName))
		{
			if (Integer.parseInt(firstName) == -1)
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * ************************************************** 
	 * Method  : extractDouble
	 * Purpose : A convenience method That ensures that the User input is a
	 * double and prompts them to enter one if they do not enter a double
	 * 
	 * @param scanner
	 *            the scanner to listen to input from
	 * @return the double that the user (eventually) enters
	 */
	private double nextDouble(Scanner scanner)
	{
		while (true)
		{
			String choiceString = scanner.next();
			if (isDouble(choiceString))
			{
				return Double.parseDouble(choiceString);
			}
			else
			{
				System.out.println("Please enter a number");
			}
		}
	}

	/**
	 * ************************************************** 
	 * Method  : extractInt
	 * Purpose : A convenience method that ensures that the User enters an
	 * integer, if they do not enter An integer it prompts them to enter one
	 * 
	 * @param scanner
	 *            the scanner to listen to input from
	 * @return the integer that the user (eventually) enters
	 */
	private int nextInt(Scanner scanner)
	{
		while (true)
		{
			String choiceString = scanner.nextLine();
			if (isInt(choiceString))
			{
				return Integer.parseInt(choiceString);
			}
			else
			{
				// Scanner occasionally gets blank line events,
				// I trim them out with this if
				if (!choiceString.equals(""))
				{
					System.out.println("Please enter a number");
				}
			}
		}
	}

	/**
	 * ************************************************** 
	 * Method : isInt P
	 * Purpose: tests if a given string is actually an integer
	 * 
	 * @param string
	 *            the string to test
	 * @return a boolean representing the answer to the question: is this String
	 *         an Integer?
	 */
	private boolean isInt(String string)
	{
		try
		{
			Integer.parseInt(string);
			return true;
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}

	/**
	 * ************************************************** 
	 * Method  : isDouble
	 * Purpose : tests if a given string is a double
	 * 
	 * @param string
	 *            the string to test
	 * @return represents the answer to the question: is this String a double?
	 */
	private boolean isDouble(String string)
	{
		try
		{
			Double.parseDouble(string);
			return true;
		}
		catch (NumberFormatException e)
		{
			return false;
		}
	}
}
