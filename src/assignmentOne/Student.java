/********************************************************
 * Project: Arraylist Of Students
 * File: Student.java 
 * Name: Trenton Maki 
 * Date: 9/29/13 
 * Description: This is the data definition for students. Students
 * can have a first name, last name
 ********************************************************/
package assignmentOne;

public class Student
{
	private String firstName;
	private String lastName;
	private int sNumber;
	private String major;
	private double gpa;
	private static int count = 0;

	/**
	 * Create an unknown student with default values
	 */
	public Student()
	{
		firstName = "Uknown";
		lastName = "Uknown";
		sNumber = -1;
		major = "Unknown";
		gpa = -1;
		count++;
	}

	/**
	 * Create a student with the given data
	 * 
	 * @param firstName
	 *            the first name of the student
	 * @param lastName
	 *            the last name of the student
	 * @param major
	 *            the major of the student
	 * @param gpa
	 *            the gpa of the student
	 * @param sNumber
	 *            the sNumber of the student
	 */
	public Student(String firstName, String lastName, String major, double gpa,
			int sNumber)
	{
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.sNumber = sNumber;
		this.major = major;
		this.gpa = gpa;
		count++;
	}

	/**
	 * ************************************************** 
	 * Method  : getFirstName
	 * Purpose : get the first name of the student
	 * 
	 * @return the first name of the student
	 */
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * ************************************************** Method : setFirstName
	 * Purpose : set the first name of the student
	 * 
	 * @param firstName
	 *            the new first name of the student
	 */
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * ************************************************** 
	 * Method : getLastName
	 * Purpose : get the last name of the student
	 * 
	 * @return the last name of the student
	 */
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * ************************************************** 
	 * Method : setLastName
	 * Purpose : reset the last name of the student
	 * 
	 * @param lastName
	 *            the new last name of the student
	 */
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * ************************************************** 
	 * Method : getsNumber
	 * Purpose : get the sNumber of the student
	 * 
	 * @return the sNumber of the student
	 */
	public int getsNumber()
	{
		return sNumber;
	}

	/**
	 * ************************************************** 
	 * Method : setSNumber
	 * Purpose : set the sNumber of the student
	 * 
	 * @param sNumber
	 *            the new sNumber of the student
	 */
	public void setSNumber(int sNumber)
	{
		this.sNumber = sNumber;
	}

	/**
	 * ************************************************** 
	 * Method : getMajor
	 * Purpose : returns the current major of the student
	 * 
	 * @return the current major of the student
	 */
	public String getMajor()
	{
		return major;
	}

	/**
	 * **************************************************
	 * Method : setMajor
	 * Purpose : set the major of the student
	 * 
	 * @param major
	 *            the new major of the student
	 */
	public void setMajor(String major)
	{
		this.major = major;
	}

	/**
	 * **************************************************
	 * Method : getGpa
	 * Purpose : returns the current GPA of the student
	 * 
	 * @return the current GPA of the student
	 */
	public double getGpa()
	{
		return gpa;
	}

	/**
	 * ************************************************** 
	 * Method : setGpa
	 * Purpose : set the GPA of the student
	 * 
	 * @param gpa
	 *            the number to set the GPA to
	 */
	public void setGpa(double gpa)
	{
		this.gpa = gpa;
	}

	/**
	 * ************************************************** 
	 * Method : getCount
	 * Purpose : returns the current number of student instances
	 * 
	 * @return the current number of student instances
	 */
	public static int getCount()
	{
		return count;
	}

	/**
	 * ************************************************** 
	 * Method : setCount
	 * Purpose : sets the current number of students to count
	 * 
	 * @param count
	 *            the number to set count to, this is here to manage the
	 *            subtraction of student instances others must track the
	 *            deletion of student instances and call this method to reset it
	 */
	public static void setCount(int count)
	{
		Student.count = count;
	}

	@Override
	public String toString()
	{
		return "Student: " + firstName + " " + lastName + ", sNumber: "
				+ sNumber + ", major: " + major + ", gpa: " + gpa;
	}
}
