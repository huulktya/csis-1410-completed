/********************************************************
 * Project 	   : Arraylist Of Students
 * File 	   : StudentList.java
 * Name		   : Trenton Maki
 * Date 	   : 9/29/13
 * Description : This class is a wrapper for the Arraylist class.
 * It offers expanded functionality such as additional methods of finding
 * students (by name, by sNumber etc)
 ********************************************************/
package assignmentOne;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StudentList
{
	private ArrayList<Student> studentList;
	private Map<String, Student> mapOfNames;
	private Map<String, Student> mapOfFirstNames;
	private Map<String, Student> mapOfLastNames;
	private Map<String, Student> mapOfSNumber;

	/**
	 * The constructor for StudentList
	 */
	public StudentList()
	{
		studentList = new ArrayList<Student>();
		mapOfNames = new HashMap<String, Student>();
		mapOfSNumber = new HashMap<String, Student>();
		mapOfFirstNames = new HashMap<String, Student>();
		mapOfLastNames = new HashMap<String, Student>();
		mapOfNames = new HashMap<String, Student>();
	}

	/****************************************************
	 * Method : addStudent 
	 * 
	 * Purpose : adds the student to the list of students.
	 * If there are duplicates it overwrites the original
	 * 
	 * @param student
	 *            the student to add to the list
	 ****************************************************/
	public void addStudent(Student student)
	{
		studentList.add(student);
		mapOfFirstNames.put(student.getFirstName(), student);
		mapOfLastNames.put(student.getLastName(), student);

		String name = student.getFirstName() + " " + student.getLastName();
		mapOfNames.put(name, student);
		mapOfSNumber.put(student.getsNumber() + "", student);
	}

	/****************************************************
	 * Method : findStudent 
	 * Purpose : Get a student from the list, Supports finding the
	 * student by full name, first name, last name, and sNumber
	 * 
	 * @param studentID
	 *            the students ID, may be the student's SNumber, full name,
	 *            first name, or last name
	 * @return The student associated with the student ID
	 ****************************************************/
	public Student findStudent(String studentID)
	{
		Student student = getStudent(studentID);
		int indexOfStudent = studentList.indexOf(student);
		if (indexOfStudent == -1)
		{
			return null;
		}
		return studentList.get(indexOfStudent);
	}

	/**
	 * **************************************************
	 * Method  : getStudent
	 * Purpose : returns the first student found that matches the given ID A
	 * convenience method for this class
	 * 
	 * @param studentID
	 *            the ID of the student, may be the first name, last name, full
	 *            name, or sNumber of the student
	 * @return the first student found that matches the ID, returns null if no
	 *         student found
	 ****************************************************/
	private Student getStudent(String studentID)
	{
		Student student = mapOfNames.get(studentID);
		if (student == null)
		{
			student = mapOfSNumber.get(studentID);
			if (student == null)
			{
				student = mapOfFirstNames.get(studentID);
				if (student == null)
				{
					student = mapOfLastNames.get(studentID);
				}
			}
		}
		return student;
	}

	/****************************************************
	 * Method  : deleteStudent 
	 * Purpose : deletes the student from the list
	 * 
	 * @param studentID the ID of the student to delete, 
	 * can be a students full name, first name, last name,
	 * or sNumber
	 ****************************************************/
	public boolean deleteStudent(String studentID)
	{
		Student student = getStudent(studentID);
		if (student == null)
		{
			return false;
		}
		else
		{
			mapOfFirstNames.remove(student.getFirstName());
			mapOfLastNames.remove(student.getLastName());
			mapOfNames.remove(student.getFirstName() + " "
					+ student.getLastName());
			mapOfSNumber.remove(student.getsNumber());
			studentList.remove((studentList.indexOf(student)));
			Student.setCount(Student.getCount() - 1);
			return true;
		}
	}

	/****************************************************
	 * Method  : getAllStudents
	 * Purpose : Returns a List of all Students in this
	 * instance
	 * 
	 * @return A list of all students currently in this instance
	 ****************************************************/
	public ArrayList<Student> getAllStudents()
	{
		return studentList;
	}

	/****************************************************
	 * Method  : getTotalNumberOfStudents 
	 * Purpose : Gives you the total number of
	 * students
	 * 
	 * @return the current total number of students
	 ****************************************************/
	public int getTotalNumberOfStudents()
	{
		return Student.getCount();
	}
}
