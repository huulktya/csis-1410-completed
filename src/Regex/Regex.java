package Regex;
/********************************************************
 * Project : CSIS1410
 * File : Regex.java 
 * Name : Trenton Maki, Jose Pando Mendez
 * Date : Oct 21, 2013
 * Description : A Email, Phone Number, and zip code
 * validator
 ********************************************************/
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Regex extends JFrame
{

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					Regex frame = new Regex();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Regex()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 585, 69);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnValidatePhoneNumber = new JButton("Validate Phone Number");
		btnValidatePhoneNumber.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				boolean successful = false;
				while (!successful)
				{
					String input = JOptionPane
							.showInputDialog("Enter a Phone Number to validate (Format: (xxx) yyy-zzzz)");
					try
					{
						if (input != null)
						{
							validatePhoneNumber(input);
							JOptionPane.showMessageDialog(Regex.this,
									"That was a valid Phone Number");
							successful = true;
						}
						else
						{
							break;
						}
					}
					catch (IllegalArgumentException ex)
					{
						JOptionPane.showMessageDialog(Regex.this,
								"Sorry, invalid Phone Number");
					}
				}
			}
		});
		contentPane.add(btnValidatePhoneNumber);

		JButton btnValidateEmail = new JButton("Validate Email");
		btnValidateEmail.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{

				boolean successful = false;
				while (!successful)
				{
					String input = JOptionPane
							.showInputDialog("Enter an email to validate");
					try
					{
						if (input != null)
						{
							validateEmail(input);
							JOptionPane.showMessageDialog(Regex.this,
									"That was a valid email");
							successful = true;
						}
						else
						{
							break;
						}
					}
					catch (IllegalArgumentException ex)
					{
						JOptionPane.showMessageDialog(Regex.this,
								"Sorry, invalid email");
					}
				}
			}
		});
		contentPane.add(btnValidateEmail);

		JButton btnValidateZipCode = new JButton("Validate Zip Code");
		btnValidateZipCode.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				boolean successful = false;
				while (!successful)
				{
					String input = JOptionPane
							.showInputDialog("Enter a Zip Code to validate");
					try
					{
						if (input != null)
						{
							validateZipCode(input);
							JOptionPane.showMessageDialog(Regex.this,
									"That was a valid Zip Code");
							successful = true;
						}
						else
						{
							break;
						}
					}
					catch (IllegalArgumentException ex)
					{
						JOptionPane.showMessageDialog(Regex.this,
								"Sorry, invalid zip code");
					}
				}
			}

		});
		contentPane.add(btnValidateZipCode);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				exit();
			}
		});
		contentPane.add(btnExit);
	}

	/****************************************************
	 * Method : validateEmail Purpose : determines if an email address is valid
	 * 
	 * @param input
	 *            the input to validate
	 * @throws IllegalArgumentException
	 *             thrown if input is invalid
	 ****************************************************/
	protected void validateEmail(String input) throws IllegalArgumentException
	{
		if (!input
				.matches("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"))
		{
			throw new IllegalArgumentException();
		}

	}

	/****************************************************
	 * Method : validatePhoneNumber 
	 * Purpose : Validate a phone number
	 * 
	 * @param input
	 *            the input to validate
	 * @throws IllegalArgumentException
	 *             thrown if input is invalid
	 ****************************************************/
	protected void validatePhoneNumber(String input)
			throws IllegalArgumentException
	{
		// (xxx) yyy-zzzz
		if (!input.matches("\\([\\d]{3}\\) [\\d]{3}-[\\d]{4}"))
		{
			throw new IllegalArgumentException();
		}
	}

	/**
	 * ************************************************** 
	 * Method : validateZipCode 
	 * Purpose : Validate a zip code
	 * 
	 *@param input the input to validate
	 * @throws IllegalArgumentException thrown if input is invalid
	 **************************************************
	 */
	private void validateZipCode(String input) throws IllegalArgumentException
	{
		// 1111-2222
		// 1111 1111
		// 2222
		if (!input.matches("[\\d]{4}[\\s-][\\d]{4}?"))
		{
			throw new IllegalArgumentException();
		}
	}

	/**
	 * ************************************************** 
	 * Method : exit 
	 * Purpose : exit the JFrame 
	 * **************************************************
	 */
	private void exit()
	{
		this.dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
	}

}