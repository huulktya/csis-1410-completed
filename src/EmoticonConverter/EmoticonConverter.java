package EmoticonConverter;
/********************************************************
 *
 *  Project :  CSIS1410
 *  File    :  EmoticonConverter.java
 *  Name    :  Trenton Maki
 *  Date    :  Oct 18, 2013
 *
 *  Description : The UI for the Emoticon Converter assignment
 *
 ********************************************************/
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class EmoticonConverter extends JFrame
{
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					EmoticonConverter frame = new EmoticonConverter();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EmoticonConverter()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 225);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));
		setLocationRelativeTo(null);
		
		JButton smileyButton = new JButton("Display :)");
		smileyButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				displayFrame(new smileyPanel());
			}
		});
		contentPane.add(smileyButton);
		
		JButton angryButton = new JButton("Display >:(");
		angryButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				displayFrame(new angryPanel());
			}
		});
		contentPane.add(angryButton);
		
		JButton winkingButton = new JButton("Display  ;)");
		winkingButton.addActionListener(new ActionListener()
		{
			
			@Override
			public void actionPerformed(ActionEvent e)
			{
				displayFrame(new winkingPanel());
			}

		});
		contentPane.add(winkingButton);
	}

	private void displayFrame(Component c)
	{
		JFrame frame = new JFrame();
		frame.setSize(100, 100);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.add(c);
		frame.setVisible(true);
	}

}
