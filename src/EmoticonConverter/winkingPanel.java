package EmoticonConverter;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Arc2D;

 /********************************************************
 *
 *  Project :  CSIS1410
 *  File    :  winkingPanel.java
 *  Name    :  Trenton Maki
 *  Date    :  Oct 18, 2013
 *
 *  Description : A panel that draws ;)
 *
 ********************************************************/


public class winkingPanel extends Component
{
	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		int width = getWidth();
		int height = getHeight();
		//Fill in the background
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		//fill in the face
		g.setColor(Color.YELLOW);
		g.fillOval(5, 5, width-10, height-10);
		//eyes
		g.setColor(Color.BLACK);
		g.fillOval(width/5, height/4, width/5, height/5);
		g.fillOval((width*2)/3, height/4 + (int)(height*1d/10d), width/5, height/20);
		//Mouth
		Arc2D mouth = new Arc2D.Double(width/4d, height*(3d/5d), width*3d/4d - width/5d, height*3d/4d - height/2d, 225, 90, Arc2D.OPEN);
		Graphics2D g2d = (Graphics2D)g;
		Stroke s = g2d.getStroke();
		g2d.setStroke(new BasicStroke(height/15));
		g2d.draw(mouth);
		g2d.setStroke(s);
	}
}
