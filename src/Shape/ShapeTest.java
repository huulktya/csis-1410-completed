package Shape;
/********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  ShapeTest.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A class that tests the shapes I have made 
 *
 ********************************************************/
import java.util.ArrayList;
import java.util.List;


public class ShapeTest
{
	public static void main(String... args) 
	{
		Shape circle = new Circle(5);
		Shape square = new Square(6);
		Shape triangle = new Triangle(5, 6, 7);
		Shape sphere = new Sphere(8);
		Shape cube = new Cube(9);
		Shape tetrahedron = new Tetrahedron(10);
		List<Shape> shapeList = new ArrayList<>();
		shapeList.add(circle);
		shapeList.add(square);
		shapeList.add(triangle);
		shapeList.add(sphere);
		shapeList.add(cube);
		shapeList.add(tetrahedron);
		
		for (Shape shape : shapeList)
		{
			System.out.println(shape);
		}
	}
}
