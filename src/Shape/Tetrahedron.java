package Shape;
 /********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  Tetrahedron.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A tetrahedron data definition
 *
 ********************************************************/

/**
 * @author Huulktya
 *
 */
public class Tetrahedron extends ThreeDimensionalShape
{

	private double side;
	
	public Tetrahedron(double side)
	{	
		this.side = side;
	}
	
	public double getSide()
	{
		return side;
	}

	public void setSide(double side)
	{
		this.side = side;
	}

	@Override
	public double getVolume()
	{
		return (Math.pow(side, 3))/(6 * Math.pow(2, .5));
	}
	
	@Override
	public double getArea()
	{
		return Math.pow(3, .5) * Math.pow(side, 2);
	}

	@Override
	public String toString()
	{
		return String.format("A tetrahedron with side length: %f, area: %f, and volume: %f", side, getArea(), getVolume());
	}
	
}
