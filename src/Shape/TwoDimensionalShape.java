package Shape;
 /********************************************************
 *
 *  Project :  CSIS1410
 *  File    :  twoDimensionalShape.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A superclass for 2D shapes
 *
 ********************************************************/


public abstract class TwoDimensionalShape extends Shape
{
}
