package Shape;
 /********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  Shape.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A basic shape super class
 *
 ********************************************************/

public abstract class Shape
{	
	public abstract double getArea();
	
}
