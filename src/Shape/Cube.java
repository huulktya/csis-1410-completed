package Shape;
 /********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  Cube.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A cube Data Definition
 *
 ********************************************************/


public class Cube extends ThreeDimensionalShape
{

	private double side;

	public Cube(double side)
	{
		this.side = side;
	}


	public double getSide()
	{
		return side;
	}


	public void setSide(double side)
	{
		this.side = side;
	}


	@Override
	public double getVolume()
	{
		return Math.pow(side, 3);
	}

	
	@Override
	public double getArea()
	{
		return 6 * Math.pow(side, 2);
	}
	
	
	@Override
	public String toString()
	{
		return String.format("A square with side length: %f, area: %f, and volume: %f", side, getArea(), getVolume());
	}

}
