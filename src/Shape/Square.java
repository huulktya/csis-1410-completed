package Shape;
 /********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  Square.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A square data definition
 *
 ********************************************************/

public class Square extends TwoDimensionalShape
{
	private double s;

	public Square(double s)
	{
		this.s = s;
	}

	public double getS()
	{
		return s;
	}

	public void setS(double s)
	{
		this.s = s;
	}

	@Override
	public double getArea()
	{
		return Math.pow(s, 2);
	}

	@Override
	public String toString()
	{
		return "A square with side length: " + s + " and area: " + getArea();
	}
	
}
