package Shape;
 /********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  Triangle.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A triangle data definition 
 *  
 ********************************************************/

public class Triangle extends TwoDimensionalShape
{

	private double s1;
	private double s2;
	private double s3;
	
	public Triangle(double s1, double s2, double s3)
	{
		this.s1 = s1;
		this.s2 = s2;
		this.s3 = s3;
	}

	public double getS1()
	{
		return s1;
	}

	public void setS1(double s1)
	{
		this.s1 = s1;
	}

	public double getS2()
	{
		return s2;
	}

	public void setS2(double s2)
	{
		this.s2 = s2;
	}

	public double getS3()
	{
		return s3;
	}

	public void setS3(double s3)
	{
		this.s3 = s3;
	}

	@Override
	public double getArea()
	{	
		double p = (s1 + s2 + s3)/2;
		return Math.sqrt(p * (p - s1) * (p - s2) * (p - s3));
	}

	@Override
	public String toString()
	{
		return String.format("A triangle with side lengths: %f, %f, %f and area: %f", s1, s2, s3, getArea());
	}

}
