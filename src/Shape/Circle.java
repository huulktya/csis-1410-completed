package Shape;
 /********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  Circle.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A circle data definition.
 *
 ********************************************************/

public class Circle extends TwoDimensionalShape
{
	private double radius;
	
	public Circle(double radius)
	{
		this.radius = radius;
	}
	
	public double getRadius()
	{
		return radius;
	}

	public void setRadius(double radius)
	{
		this.radius = radius;
	}

	@Override
	public double getArea()
	{
		return Math.PI * (Math.pow(radius, 2));
	}

	
	@Override
	public String toString()
	{
		return "A circle with radius: " + radius + " and area: " + getArea();
	}

}
