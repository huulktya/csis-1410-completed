package Shape;

 /********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  Sphere.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A sphere data definition
 *
 ********************************************************/

public class Sphere extends ThreeDimensionalShape
{

	private double radius;
	
	public Sphere(double radius)
	{
		this.radius = radius;
	}
	
	public double getRadius()
	{
		return radius;
	}

	public void setRadius(double radius)
	{
		this.radius = radius;
	}

	@Override
	public double getVolume()
	{
		return (4d/3d) * Math.PI * (Math.pow(radius, 3));
	}

	
	@Override
	public double getArea()
	{
		return 4 * Math.PI * (Math.pow(radius, 2));
	}
	
	@Override
	public String toString()
	{
		return String.format("A sphere with radius: %f, area: %f, and volume: %f", radius, getArea(), getVolume());
	}
	
}
