package Shape;
 /********************************************************
 *
 *  Project :  Polymorphism Lab
 *  File    :  ThreeDimensionalShape.java
 *  Name    :  Trenton Maki
 *  Date    :  Sep 23, 2013
 *
 *  Description : A basic superclass for 3D shapes
 *
 ********************************************************/

public abstract class ThreeDimensionalShape extends Shape
{
	public abstract double getVolume();
}
