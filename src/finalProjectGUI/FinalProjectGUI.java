package finalProjectGUI;
/********************************************************
 * Project : CSIS1410 
 * File : FinalProjectGUI.java 
 * Name : Trenton Maki
 * Date : Oct 14, 2013 
 * Description : Version .1 of my final project GUI
 ********************************************************/
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;


public class FinalProjectGUI extends JFrame
{
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField textField;
	private JTextField addressField_1;
	private JTextField addressField_2;
	private JTextField cityField;
	private JTextField zipCodeField;
	private JTextField companyTextField;
	private JTextField workPhoneField;
	private JTextField emailField;
	private JTextField relationshipField;
	private JTextField birthDateField;
	private String[] states = { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI",
			"ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA",
			"MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM",
			"NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD",
			"TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY" };

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					FinalProjectGUI frame = new FinalProjectGUI();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FinalProjectGUI()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 600);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		JMenuItem openMenuItem = new JMenuItem("Open");
		fileMenu.add(openMenuItem);

		JMenuItem saveMenuItem = new JMenuItem("Save");
		fileMenu.add(saveMenuItem);

		JMenuItem updateMenuItem = new JMenuItem("Update");
		fileMenu.add(updateMenuItem);
		getContentPane().setLayout(
				new MigLayout("", "[][grow][grow]", "[][][][grow][grow][][][][grow][][][][]"));

		JLabel lblFirstName = new JLabel("First Name:");
		getContentPane().add(lblFirstName, "cell 0 0,alignx left");

		firstNameField = new JTextField();
		getContentPane().add(firstNameField, "cell 1 0,alignx left");
		firstNameField.setColumns(10);

		JLabel lblLastName = new JLabel("Last Name:");
		getContentPane().add(lblLastName, "cell 0 1,alignx left");

		lastNameField = new JTextField();
		getContentPane().add(lastNameField, "cell 1 1,alignx left");
		lastNameField.setColumns(10);

		JLabel lblPhone = new JLabel("Phone:");
		getContentPane().add(lblPhone, "cell 0 2,alignx left");

		textField = new JTextField();
		getContentPane().add(textField, "cell 1 2,alignx left");
		textField.setColumns(10);

		JLabel lblAddress_1 = new JLabel("Address (1):");
		getContentPane().add(lblAddress_1, "cell 0 3,alignx left");

		addressField_1 = new JTextField();
		getContentPane().add(addressField_1, "cell 1 3,growx");
		addressField_1.setColumns(10);

		JLabel lblAddress_2 = new JLabel("Address (2):");
		getContentPane().add(lblAddress_2, "cell 0 4,alignx left");

		addressField_2 = new JTextField();
		getContentPane().add(addressField_2, "cell 1 4,growx");
		addressField_2.setColumns(10);

		JLabel lblCity = new JLabel("City:");
		getContentPane().add(lblCity, "cell 0 5,alignx left");

		cityField = new JTextField();
		getContentPane().add(cityField, "cell 1 5,alignx left");
		cityField.setColumns(10);

		JLabel lblState = new JLabel("State:");
		getContentPane().add(lblState, "cell 0 6,alignx left");

		//JComboBox<String> stateComboBox = new JComboBox<String>(states); This one isn't allowed since WindowBuilder
		//getContentPane().add(stateComboBox, "cell 1 6,alignx left"); 	   doesn't recognize Java 7. Sadly I must do it 
		//																   incorrectly
		JComboBox stateComboBox = new JComboBox(states);
		getContentPane().add(stateComboBox, "cell 1 6,alignx left");
				

		JLabel lblZipCode = new JLabel("Zip Code:");
		getContentPane().add(lblZipCode, "cell 0 7,alignx left");

		zipCodeField = new JTextField();
		getContentPane().add(zipCodeField, "cell 1 7,alignx left");
		zipCodeField.setColumns(10);

		ImageIcon icon = new ImageIcon(getClass().getResource("image.png"));
		JLabel lblImgaeLabel = new JLabel(icon);
		getContentPane().add(lblImgaeLabel, "flowx,cell 0 8");

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, "cell 1 8 2 4,grow");

		JPanel bussinessPanel = new JPanel();
		tabbedPane.addTab("Business", null, bussinessPanel, null);
		bussinessPanel.setLayout(new MigLayout("", "[][][][grow]", "[][][]"));

		JLabel lblCompany = new JLabel("Company:");
		bussinessPanel.add(lblCompany, "cell 2 0,alignx left");

		companyTextField = new JTextField();
		bussinessPanel.add(companyTextField, "cell 3 0,alignx left");
		companyTextField.setColumns(10);

		JLabel lblWorkPhone = new JLabel("Work Phone:");
		bussinessPanel.add(lblWorkPhone, "cell 2 1,alignx left");

		workPhoneField = new JTextField();
		bussinessPanel.add(workPhoneField, "cell 3 1,alignx left");
		workPhoneField.setColumns(10);

		JLabel lblEmail = new JLabel("Email:");
		bussinessPanel.add(lblEmail, "cell 2 2,alignx left");

		emailField = new JTextField();
		bussinessPanel.add(emailField, "cell 3 2,alignx left");
		emailField.setColumns(10);

		JPanel familyPanel = new JPanel();
		tabbedPane.addTab("Family", null, familyPanel, null);
		familyPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JLabel lblRelationship = new JLabel("Relationship:");
		familyPanel.add(lblRelationship);

		relationshipField = new JTextField();
		familyPanel.add(relationshipField);
		relationshipField.setColumns(10);

		JLabel lblBirthDate = new JLabel("Birth Date:");
		familyPanel.add(lblBirthDate);

		birthDateField = new JTextField();
		familyPanel.add(birthDateField);
		birthDateField.setColumns(10);

		JPanel friendPanel = new JPanel();
		tabbedPane.addTab("Friend", null, friendPanel, null);
		friendPanel.setLayout(new MigLayout("", "[grow]", "[][grow]"));

		JLabel lblNewLabel = new JLabel("Info:");
		friendPanel.add(lblNewLabel, "cell 0 0,alignx center");

		JTextArea infoTextArea = new JTextArea();
		friendPanel.add(infoTextArea, "cell 0 1,grow");

		JButton btnSubmit = new JButton("Submit");
		getContentPane().add(btnSubmit, "cell 1 12,alignx center");

	}

}
