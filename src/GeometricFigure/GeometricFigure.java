package GeometricFigure;
/********************************************************
 * Project : CSIS1410 
 * File : GeometricFigure.java 
 * Name : Trenton Maki 
 * Date : Oct 16, 2013 
 * Description : Creates a frame that has four quadrants and, in each one,
 * it draws a different shape
 ********************************************************/

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Polygon;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class GeometricFigure extends JFrame
{

	private JPanel contentPane;

	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					GeometricFigure frame = new GeometricFigure();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	public GeometricFigure()
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(2, 2, 0, 0));

		JPanel squarePanel = new FigurePanel(FigurePanel.SQUARE);
		contentPane.add(squarePanel);

		JPanel circlePanel = new FigurePanel(FigurePanel.CIRCLE);
		contentPane.add(circlePanel);

		JPanel hexagonPanel = new FigurePanel(FigurePanel.HEX);
		contentPane.add(hexagonPanel);

		JPanel FanPanel = new FigurePanel(FigurePanel.FAN);
		contentPane.add(FanPanel);
	}

}

class FigurePanel extends JPanel
{
	public static final int SQUARE = 2 ^ 1;
	public static final int CIRCLE = 2 ^ 2;
	public static final int HEX = 2 ^ 3;
	public static final int FAN = 2 ^ 4;
	private int type;

	public FigurePanel(int type)
	{
		this.type = type;
	}

	@Override
	public void paint(Graphics g)
	{
		super.paintComponent(g);
		switch (type)
		{
		case SQUARE:
			g.setColor(Color.BLUE);
			// - 20 so that I have 10 pixels on each side
			g.fillRect(10, 10, getWidth() - 20, getHeight() - 20);
			break;
		case CIRCLE:
			// I use only width so that, no matter the shape of the panel, we
			// always have a circle
			g.setColor(Color.RED);
			g.drawOval(10, 5, getWidth() - 20, getWidth() - 20);
			break;
		case HEX:
			// Hexagon drawing code courtesy of dreamincode.net user baavgai:
			// http://www.dreamincode.net/forums/user/52176-baavgai/
			// Post:
			// http://www.dreamincode.net/forums/topic/119546-drawing-a-static-sized-non-position-reliant-hexagon/
			g.setColor(Color.GREEN);
			Polygon poly = new Polygon();
			double factor = 2.0 * Math.PI / 6.0;
			for (int i = 0; i < 6; i++)
			{
				// I use (getWidth()-20) to add a 10 pixel edge on each side of
				// the hexagon
				int x = getWidth() / 2
						+ (int) ((getWidth() - 20) / 2 * Math.cos(i * factor));
				int y = getHeight() / 2
						+ (int) (getHeight() / 2 * Math.sin(i * factor));
				poly.addPoint(x, y);
			}
			g.drawPolygon(poly);
			break;
		case FAN:
			// I do: getWidth() - 20 to make sure that the fan is always
			// circular and to ensure that I have 10 pixels of space vertically
			// or horizontally
			g.setColor(Color.RED);
			g.fillArc(10, 10, getWidth() - 20, getWidth() - 20, 0, 30);
			g.setColor(Color.WHITE);
			g.fillArc(10, 10, getWidth() - 20, getWidth() - 20, 90, 30);
			g.setColor(Color.BLUE);
			g.fillArc(10, 10, getWidth() - 20, getWidth() - 20, 180, 30);
			g.setColor(Color.GREEN);
			g.fillArc(10, 10, getWidth() - 20, getWidth() - 20, 270, 30);
			break;
		}
	}
}