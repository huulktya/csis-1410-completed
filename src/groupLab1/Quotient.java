package groupLab1;
import javax.swing.JOptionPane;

/********************************************************
 * Project : CSIS1410 
 * File : Quotient.java 
 * Name : Trenton Maki, Jose Pando Mendez
 * Date : Oct 2, 2013 
 * Description : Reads in two values from the user and returns the quotient
 * of the two numbers
 ********************************************************/

public class Quotient
{
	public static void main(String... args)
	{

		String prefix = "Please enter a value for the ";
		boolean success = false;
		do
		{
			try
			{

				int numerator = getValue(prefix + "numerator");
				int denominator = getValue(prefix + "denominator");
				JOptionPane
						.showMessageDialog(
								null,
								"You entered: "
										+ numerator
										+ "/"
										+ denominator
										+ " which, using integers that are base 10, is equal to: "
										+ (numerator / denominator));
				success = true;
			}
			catch (ArithmeticException e)
			{
				JOptionPane.showMessageDialog(null,
						"Please enter a non zero number");
				e.printStackTrace();
			}
		}
		while (!success);
	}

	/****************************************************
	 * Method : getValue Purpose : Displays a JOptionPane that prompts the user
	 * with the String argument
	 * 
	 * @param queryString
	 *            the string to display
	 * @return
	 ****************************************************/
	private static int getValue(String queryString)
	{
		while (true)
		{
			try
			{
				String input = JOptionPane.showInputDialog(queryString);
				return Integer.parseInt(input);
			}
			catch (NumberFormatException e)
			{
				JOptionPane.showMessageDialog(null, "Please enter an integer");
			}
		}
	}
}
