package employee;
/********************************************************
 * Project : Lab: Polymorphic Processing
 * File : Manager.java 
 * Name : Trenton Maki, Jose Pando Mendez
 * Date : Sep 18, 2013
 * Description : A concrete implementation of the Employee abstract 
 * class. Represents a Manager of the company  
 ********************************************************/

public class Manager extends Employee
{

	private String title;
	private double golfClubDues;

	public Manager()
	{
		title = "";
		golfClubDues = 0;
	}

	public Manager(String firstName, String lastName, double pay, int idNumber,
			String title, double golfClubDues)
	{
		super(firstName, lastName, pay, idNumber);
		this.title = title;
		this.golfClubDues = golfClubDues;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public double getGolfClubDues()
	{
		return golfClubDues;
	}

	public void setGolfClubDues(double golfClubDues)
	{
		this.golfClubDues = golfClubDues;
	}

	@Override
	public double calcPay()
	{
		return getPay() - golfClubDues;
	}

	@Override
	public String toString()
	{
		return super.toString() + " Manager [title=" + title
				+ ", golfClubDues=" + golfClubDues + "]";
	}

}
