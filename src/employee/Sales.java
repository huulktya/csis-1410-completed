package employee;
/********************************************************
 * Project : Lab: Polymorphic Processing
 * File : Sales.java 
 * Name : Trenton Maki, Jose Pando Mendez
 * Date : Sep 18, 2013
 * Description : A concrete implementation of the Employee abstract 
 * class. Represents a Sales executive of the company  
 ********************************************************/
 
public class Sales extends Employee
{

	private double salesVolume;
	private double commissionRate;

	public Sales() {
		salesVolume = 0;
		commissionRate = 0;
	}
	
	public Sales(String firstName, String lastName, double pay, int idNumber,
			double salesVolume, double commissionRate)
	{
		super(firstName, lastName, pay, idNumber);
		this.salesVolume = salesVolume;
		this.commissionRate = commissionRate;
	}

	public double getSalesVolume()
	{
		return salesVolume;
	}

	public void setSalesVolume(double salesVolume)
	{
		this.salesVolume = salesVolume;
	}

	public double getCommissionRate()
	{
		return commissionRate;
	}

	public void setCommissionRate(double commissionRate)
	{
		this.commissionRate = commissionRate;
	}


	@Override
	public double calcPay()
	{
		super.setPay(commissionRate*salesVolume);
		return super.getPay();
	}

	@Override
	public String toString()
	{
		return super.toString() + " Sales [salesVolume=" + salesVolume + ", commissionRate="
				+ commissionRate + "]";
	}

}
