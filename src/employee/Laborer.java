package employee;
/********************************************************
 * Project : Lab: Polymorphic Processing
 * File : Laborer.java 
 * Name : Trenton Maki, Jose Pando Mendez
 * Date : Sep 18, 2013
 * Description : A concrete implementation of the Employee abstract
 *  class. Represents a laborer
 ********************************************************/

public class Laborer extends Employee
{
	private int hoursWorked;
	private double hourlyRate;

	public Laborer() {
		hoursWorked = 0;
		hourlyRate = 0;
	}
	
	public Laborer(String firstName, String lastName, double pay, int idNumber,
			int hoursWorked, double payPerHour)
	{
		super(firstName, lastName, pay, idNumber);
		this.hoursWorked = hoursWorked;
		this.hourlyRate = payPerHour;
	}

	public int getHoursWorked()
	{
		return hoursWorked;
	}

	public void setHoursWorked(int hoursWorked)
	{
		this.hoursWorked = hoursWorked;
	}

	public double getPayPerHour()
	{
		return hourlyRate;
	}

	public void setPayPerHour(double payPerHour)
	{
		this.hourlyRate = payPerHour;
	}

	@Override
	public String toString()
	{
		return super.toString() + " Laborer [hoursWorked=" + hoursWorked + ", payPerHour="
				+ hourlyRate + "]";
	}

	@Override
	public double calcPay()
	{
		super.setPay(hoursWorked * hourlyRate);
		return super.getPay();
	}

}
