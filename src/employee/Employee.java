package employee;
/********************************************************
 * Project : Lab: Polymorphic Processing
 * File : Employee.java 
 * Name : Trenton Maki, Jose Pando Mendez
 * Date : Sep 18, 2013
 * Description : An abstract representation of an employee
 ********************************************************/

public abstract class Employee
{
	private String firstName;
	private String lastName;
	private double pay;
	private int idNumber;

	public Employee()
	{
		firstName = "";
		lastName = "";
		pay = 0;
		idNumber = -1;
	}

	public Employee(String firstName, String lastName, double pay, int idNumber)
	{
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.pay = pay;
		this.idNumber = idNumber;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public double getPay()
	{
		return pay;
	}

	public void setPay(double pay)
	{
		this.pay = pay;
	}

	public int getIdNumber()
	{
		return idNumber;
	}

	public void setIdNumber(int idNumber)
	{
		this.idNumber = idNumber;
	}

	public abstract double calcPay();

	@Override
	public String toString()
	{
		return "Employee [firstName=" + firstName + ", lastName=" + lastName
				+ ", pay=" + pay + ", idNumber=" + idNumber + "]";
	}

}
