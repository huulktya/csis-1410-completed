package employee;
/********************************************************
 * Project : Lab: Polymorphic Processing 
 * File : EmployeeTest.java 
 * Name : Trenton
 * Maki, Jose Pando Mendez 
 * Date : Sep 18, 2013 
 * Description : An example of polymorphic programming.
 ********************************************************/

import java.util.ArrayList;

public class EmployeeTest
{

	public static void main(String... args)
	{
		ArrayList<Employee> eList = new ArrayList<>();
		Manager m1 = new Manager("Jose", "Mendez", 9001, 1337,
				"Ruler of Darkness", -100); // They pay him to golf there
		Manager m2 = new Manager("Trenton", "Maki", 9002, 42,
				"Dark Lord of the Sith", 0); // He doesn't golf
		Sales s1 = new Sales("Tom", "Cat", 100, 741, 12, 9003);
		Sales s2 = new Sales("Will E.", "Coyote", 100, 0, 3, 9004);
		Laborer l = new Laborer("Frank", "Fontaine", 200, 314159, 9, 9005);
		eList.add(s1);
		eList.add(s2);
		eList.add(m1);
		eList.add(m2);
		eList.add(l);
		for (Employee e : eList)
		{
			System.out.print(e.getFirstName() + " " + e.getLastName()
					+ ", Pay: ");
			if (e instanceof Sales)
			{
				Sales s = (Sales) e;
				double bonus = s.getSalesVolume() * .5;
				System.out.println(s.calcPay() + bonus
						+ " (Base pay plus a bonus of: " + bonus + ")");
			}
			else
			{
				System.out.println(e.calcPay());
			}
		}
	}
}
