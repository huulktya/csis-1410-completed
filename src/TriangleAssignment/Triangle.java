package TriangleAssignment;

/********************************************************
 *
 *  Project :  Inheritance Lab
 *  File    :  Triangle.java
 *  Name    :  Trenton Maki
 *  Date    :  9/16/13
 *
 *  Description : A Triangle data definition, if invalid sides
 *  are entered (sides <= 0) then the side is defaulted to 1.0
 ********************************************************/
public class Triangle extends GeometricObject
{
	private double side1;
	private double side2;
	private double side3;

	public Triangle()
	{
		side1 = 1.0;
		side2 = 1.0;
		side3 = 1.0;
	}

	public Triangle(double side1, double side2, double side3, String color,
			boolean filled)
	{
		super(color, filled);
		if (side1 <= 0)
		{
			side1 = 1.0;
		}
		else
		{
			this.side1 = side1;
		}
		if (side2 <= 0)
		{
			side2 = 1.0;
		}
		else
		{
			this.side2 = side2;
		}
		if (side3 <= 0)
		{
			side3 = 1.0;
		}
		else
		{
			this.side3 = side3;
		}
	}

	public double getSide1()
	{
		return side1;
	}

	public double getSide2()
	{
		return side2;
	}

	public double getSide3()
	{
		return side3;
	}

	@Override
	public double getArea()
	{
		double p = getPerimeter() / 2;
		return Math.sqrt(p * ((p - side1) * (p - side2) * (p - side3)));
	}

	@Override
	public double getPerimeter()
	{
		return side1 + side2 + side3;
	}

	@Override
	public String toString()
	{
		return "Triangle [side1=" + side1 + ", side2=" + side2 + ", side3="
				+ side3 + ", color=" + getColor() + ", filled=" + isFilled()
				+ "]";
	}
}
