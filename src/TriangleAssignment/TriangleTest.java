package TriangleAssignment;
/********************************************************
 *
 *  Project :  Inheritance Lab
 *  File    :  TriangleTest.java
 *  Name    :  Trenton Maki
 *  Date    :  9/16/13
 *
 *  Description : A test of the triangle data definition, as
 *  per the specification.
 ********************************************************/
public class TriangleTest {
	public static void main(String... args) {
		Triangle triangle = new Triangle(1, 1.5, 1, "yellow", true);
		System.out.println(triangle);
		System.out.println("Area: " + triangle.getArea());
		System.out.println("Perimeter: " + triangle.getPerimeter());
	}
}
