package TextIO;
/********************************************************
 *
 *  Project :  CSIS1410
 *  File    :  TextIO.java
 *  Name    :  Trenton Maki, Jose Pando Mendez
 *  Date    :  Oct 9, 2013
 *
 *  Description : This program generates a list of 100 scores between 60 and 100,
 *  writes the scores to a file called "scores.txt", reads "scores.txt", and then process it to find:
 *  * The highest score
 *  * The lowest score
 *  * The average score
 *  * The number of scores in the range 60-69
 *  * The number of scores in the range 70-79
 *  * The number of scores in the range 80-89
 *  * The number of scores in the range 90-100 
 * 	
 * Prints the results and then saves them to a file called "output.txt" 
 *
 ********************************************************/
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class TextIO
{
	private static final int NUM_OF_SCORES = 100;
	public static void main(String... args) {
		writeScores(generateScores(NUM_OF_SCORES));
		ArrayList<Integer> scores = readScores();
		int high = 0;
		int low = 100;
		int average = 0;
		int sum = 0;
		int sixtyToSeventy = 0;
		int seventyToEighty = 0;
		int eightyToNinety = 0;
		int ninetyToHundred = 0;
		for(int score : scores) {
			if(score > high) {
				high = score;
			}
			if(score < low) {
				low = score;
			}
			sum += score;
			
			if(score >= 60 && score <= 69) {
				sixtyToSeventy++;
			} else if(score >= 70 && score <= 79) {
				seventyToEighty++;
			} else if(score >= 80 && score <= 89) {
				eightyToNinety++;
			} else if(score >= 90 && score <= 100) {
				ninetyToHundred++;
			}
		}
		average = (sum/scores.size());
		System.out.println("High: "  + high);
		System.out.println("Low: " + low);
		System.out.println("Average: " + average);
		System.out.println("Numbers in 60 to 69 range: " + sixtyToSeventy);
		System.out.println("Numbers in 70 to 79 range: " + seventyToEighty);
		System.out.println("Numbers in 80 to 89 range: " + eightyToNinety);
		System.out.println("Numbers in 90 to 100 range: " + ninetyToHundred);
		
		try
		{
			PrintWriter printWriter = new PrintWriter(new File("output.txt"));
			printWriter.println("High: "  + high);
			printWriter.println("Low: " + low);
			printWriter.println("Average: " + average);
			printWriter.println("Numbers in 60 to 69 range: " + sixtyToSeventy);
			printWriter.println("Numbers in 70 to 79 range: " + seventyToEighty);
			printWriter.println("Numbers in 80 to 89 range: " + eightyToNinety);
			printWriter.println("Numbers in 90 to 100 range: " + ninetyToHundred);
			printWriter.close();
			
		}
		catch (FileNotFoundException e)
		{
			System.err.println("File not found, quitting");
			e.printStackTrace();
			System.exit(1);
		}
		
	}
	
	private static ArrayList<Integer> readScores()
	{
		ArrayList<Integer> sList = new ArrayList<>();
		try
		{
			Scanner scanner = new Scanner(new File("scores.txt"));
			while(scanner.hasNext()) {
				sList.add(scanner.nextInt());
			}
			scanner.close();
		}
		catch (FileNotFoundException e)
		{
			System.err.println("Couldn't find the file, quitting");
			e.printStackTrace();
			System.exit(1);
		}
		return sList;
	}

	private static void writeScores(ArrayList<Integer> scoreList)
	{
		File file = new File("scores.txt");
		try
		{
			PrintWriter printWriter = new PrintWriter(file);
			for(int i = 0; i < scoreList.size(); i++) {
				printWriter.print(scoreList.get(i) + " ");
			}
			printWriter.close();
		}
		catch (FileNotFoundException e)
		{
			System.err.println("Couldn't find the file, Quitting.");
			e.printStackTrace();
			System.exit(1);
		}
	}

	private static ArrayList<Integer> generateScores(int numToGenerate) {
		Random rand = new Random();
		ArrayList<Integer> sList = new ArrayList<>();
		for(int i = 0; i < numToGenerate; i++) {
			sList.add((rand.nextInt(41) + 60));
		}
		return sList;
	}
}